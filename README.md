
# Projet
Ce projet est une maquette de gestion d'une boutique de ventes d'applications avec gestion des avis
Il utilise Angular et Angular Material pour l'interface. Le serveur de donnée est le fichier db2.json géré par l'application json-server.

# Création

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Serveur de développement

Taper `ng serve` pour executer un serveur de developpement. Aller sur `http://localhost:4200/`. L'application se recharge automatiquement si un fichier source est modifié.

## Code

Taper `ng generate component component-name` pour générer un nuveau composant. On peut aussi utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Construire

Taper `ng build` pour construire le projet. Le résultat se trouve dans le répertoire `dist/`. Utiliser l'option `--prod` pour un exécutable de production.

## Tests unitaires

Taper `ng test` pour lancer des tests unitaires via [Karma](https://karma-runner.github.io).

## Tests d'intégration

Taper `ng e2e` pour lancer les tests d'intégration via [Protractor](http://www.protractortest.org/).

## Aide

Taper `ng help` pour de l'aide sur Angular ou lire le README [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
