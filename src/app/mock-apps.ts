/*
import { UneApp } from './une-app';
export const mockApps: UneApp[] =
[
    {
        "id": 1,
        "nom": "WhatApp Messenger",
        "corp": "WhatApp, inc",
        "icone": "https://lh3.googleusercontent.com/bYtqbOcTYOlgc6gqZ2rwb8lptHuwlNE75zYJu6Bn076-hTmvd96HH-6v7S0YUAAJXoJN=s128",
        "categorie": "Communication",
        "note": 5,
        "description": "WhatsApp Messenger est une application de messagerie GRATUITE disponible sur Android et autres smartphones. WhatsApp utilise la connexion Internet.",
        "screenshots": [
            "https://lh3.googleusercontent.com/rjBNILkwgNK__LCfq7ZogtLAkkNjKFAvk4rPuXJrk8XujdnFr9oK05AqvFHHkvjvGLs=w1366-h671",
            "https://lh3.googleusercontent.com/T1yhBkdEKEOWE6FvEeNhzcjMTt8FrGTlVjl8fSZzP9ojrcXVnSsbGDQT4jBjSHtpOj4=w1366-h671"
        ],
        "avis": [
            {
                "id": 1,
                "user": "Machin",
                "date": new Date("2019-08-16"),
                "note": 4,
                "description": "c'est trop de la balle"
            },
            {
                "id": 2,
                "user": "Bidule",
                "date": new Date("2019-08-16"),
                "note": 1,
                "description": "c'est pour les trous de balles"
            }
        ]
    },
    {
        "id": 2,
        "nom": "Instagram",
        "corp": "Instagram",
        "icone": "https://lh3.googleusercontent.com/2sREY-8UpjmaLDCTztldQf6u2RGUtuyf6VT5iyX3z53JS4TdvfQlX-rNChXKgpBYMw=s128",
        "categorie": "Social",
        "note": 3.5,
        "description": "Contactez vos ami(e)s, partagez vos exploits ou découvrez les dernières infos d’autres personnes à travers le monde. Explorez notre communauté",
        "screenshots": [
            "https://lh3.googleusercontent.com/ax8T6S1hQHcbeznuNVvYk4uG5o1kMbBZMKksHHLfWKmWOzNtcfLjWj845xA4fh83g7o=w1366-h671",
            "https://lh3.googleusercontent.com/ZVxqhLyEL7iyc2XdVS40XWQ41XCKmoZnto4gkloz6mV3KxOuY_Wqmmv06Z12zGKvZg=w1366-h671"
        ],
        "avis": [
            {
                "id": 1,
                "user": "TrucMuche",
                "date": new Date("2019-08-16"),
                "note": 3,
                "description": "j'aime"
            },
            {
                "id": 2,
                "user": "Chose",
                "date": new Date("2019-08-16"),
                "note": 5,
                "description": "je kiffe"
            }
        ]
    },
    {
        "id": 3,
        "nom": "Snapchat",
        "corp": "Snap, inc",
        "icone": "https://lh3.googleusercontent.com/KxeSAjPTKliCErbivNiXrd6cTwfbqUJcbSRPe_IBVK_YmwckfMRS1VIHz-5cgT09yMo=s128",
        "categorie": "Social",
        "note": 4,
        "description": "Prenez un Snap, ajoutez un texte ou des effets amusants, et envoyez-le à vos meilleurs amis.",
        "screenshots": [
            "https://lh3.googleusercontent.com/MPMJrHxpTO3nNwlaNXdeO9hAzaUg9Z2Ab475c1hDdS3URpCHHQsDUZzunjTByd75cywQ=w1366-h671",
            "https://lh3.googleusercontent.com/H03hGaiJHCqsfv460zGwPtlaAjL8WRsCov35VjGbIXsPKdRZYljQpOB5ljcbmSdahgs=w1366-h671"
        ],
        "avis": [
            {
                "id": 1,
                "user": "Dupont Lajoie",
                "date": new Date("2019-08-16"),
                "note": 1,
                "description": "Minable"
            },
            {
                "id": 2,
                "user": "Dupond Lahaine",
                "date": new Date("2019-08-16"),
                "note": 5,
                "description": "trop bon,trop bon,trop bon,trop bon,trop bon,trop bon,trop bon,trop bon"
            }
        ]
    },
    {
        "id": 4,
        "nom": "Clash Royale",
        "corp": "Supercell",
        "icone": "https://lh3.googleusercontent.com/KxeSAjPTKliCErbivNiXrd6cTwfbqUJcbSRPe_IBVK_YmwckfMRS1VIHz-5cgT09yMo=s128",
        "categorie": "Jeu",
        "note": 4.5,
        "description": "Entrez dans l'arène ! Les créateurs de Clash of Clans vous offrent un jeu multijoueur en temps réel mettant en scène des combattants royaux, vos personnages préférés de Clash of Clans et beaucoup plus encore.",
        "screenshots": [
            "https://lh3.googleusercontent.com/bvUB2E5CpgcRefVmGBdL8XtCwo2Mdwi1ea1YDixEC0_TnNSEEleuV3hFcIE_a2eomw=w1366-h671",
            "https://lh3.googleusercontent.com/VjgCHqd7Fe5MYjtRluHIBTdRMa8Rq2f5YiyoZCPbD4SlJ2LRJpBpPwpE0I02_MTnUg=w1366-h671"
        ],
        "avis": [
            {
                "id": 1,
                "user": "Durant Lajoie",
                "date": new Date("2019-08-16"),
                "note": 1,
                "description": "Bugs depuis plusieurs semaines, après une maj. Quand j'enregistre sur mon tél une photo ou une vidéo, l' appli se coupe. Il m'arrive que je n'arrive même pas à retrouver la photo sur mon téléphone donc pas génial"
            },
            {
                "id": 2,
                "user": "Durand Lahaine",
                "date": new Date("2019-08-16"),
                "note": 5,
                "description": "trop nul,trop nul,trop nul,trop nul,trop nul,trop nul,trop nul,trop nul"
            }
        ]
    },
    {
        "id": 5,
        "nom": "Facebook",
        "corp": "Facebook",
        "icone": "https://lh3.googleusercontent.com/KxeSAjPTKliCErbivNiXrd6cTwfbqUJcbSRPe_IBVK_YmwckfMRS1VIHz-5cgT09yMo=s128",
        "categorie": "Social",
        "note": 0,
        "description": "Réseau d'espionnage pour les marketeurs",
        "screenshots": [
            "https://lh3.googleusercontent.com/SyWoyThTsSv8w2Q7Vc5jjF2nMzyFwIeKoc42wxTS0HpHvsPpdoKCgQEijBj_xMLB2zA=w1366-h671",
            "https://lh3.googleusercontent.com/9laYd7oslX_Z2yn9bwD3i9lOr_RdjSc_ZJbnLE3c0lgMWjo5LTplgbsKxWQVKhR5rMM=w1366-h671"
        ],
        "avis": [
            {
                "id": 1,
                "user": "Bout d'un",
                "date": new Date("2019-08-16"),
                "note": 1,
                "description": "caca boudin"
            },
            {
                "id": 2,
                "user": "Vomito",
                "date": new Date("2019-08-16"),
                "note": 0,
                "description": "a vomir, d'ailleurs beurk"
            }
        ]
    }
];
*/
