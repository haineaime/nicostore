import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { AppService } from '../app.service';
import { Avis } from '../avis';
import { UneApp } from '../une-app';
import { User } from '../user';

export interface AvisV {
  Action: string;
  Numero: number;
  Utilisateurs: string;
  Applications: string;
  Date: Date;
  Note: number;
  Description: string;
}

@Component({
  selector: 'app-liste-avis',
  templateUrl: './liste-avis.component.html',
  styleUrls: ['./liste-avis.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListeAvisComponent implements OnInit {

  listeAvisV: AvisV[];
  listeAvis: Avis[];
  users: User[];
  apps: UneApp[];

  expandedElement: AvisV | null;
  
  selectedAvis: Avis;
  displayAvis = false;
  manageAvis = false;
  done = false;

  typeForm = '';
  displayedColumns: string[] = ['Action', 'Numero', 'Utilisateurs', 'Applications', 'Date', 'Note'];  // , 'Description'

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.refreshListeAvis();
  }

  refreshListeAvis() {
    this.done = false;
    this.displayAvis = false;
    this.manageAvis = false;
    this.selectedAvis = null;
    this.typeForm = '';
	
	const userPromise = this.appService.getUsers().toPromise();
	const appsPromise = this.appService.getApps().toPromise();
	Promise.all([userPromise,appsPromise]).then( (val) => {
	  this.users = val[0];
	  this.apps = val[1];
	});

    this.appService.getListeAvis().subscribe(
      result => {
        this.listeAvisV = [];
        this.listeAvis = result;

        for (const a of this.listeAvis) {
          let resultUser = '';
          let resultApp = '';
          for (const u of this.users) {
            if (u.id === a.usersId) {
              resultUser = u.nom;
            }
          }
          for (const ap of this.apps) {
            if (ap.id === a.appsId) {
              resultApp = ap.nom;
            }
          }
          this.listeAvisV.push({
			Action: 'Action',
            Numero: a.id,
            Utilisateurs: resultUser,
            Applications: resultApp,
            Date: a.date,
            Note: a.note,
            Description: a.description
          });
          this.done = true;
        }
      },
      error => console.error('Une erreur est survenue', error)
    );
  }

  selectAvis(a: Avis): void {
    this.displayAvis = true;
    this.manageAvis = false;
    this.selectedAvis = a;
  }
  addAvis() {
    this.displayAvis = false;
    this.manageAvis = true;
    this.typeForm = 'add';
    this.selectedAvis = new Avis();
  }
  editAvis(av: AvisV) {
    this.displayAvis = false;
    this.manageAvis = true;
    this.typeForm = 'edit';
    this.selectedAvis = this.listeAvis.find( a => { return a.id === av.Numero; });
  }
  deleteAvis(av: AvisV) {
	let avis = this.listeAvis.find( a => { return a.id === av.Numero; });
    this.appService.deleteAvis(avis).subscribe(
      () => this.refreshListeAvis()
    );
  }
  onManageAvis(a: Avis) {
    this.refreshListeAvis();
    this.selectAvis(a);
  }

}
