import { Component, OnInit, Input } from '@angular/core';

import { AppService } from '../app.service';
import { UneApp } from '../une-app'

@Component({
  selector: 'app-info-app',
  templateUrl: './info-app.component.html',
  styleUrls: ['./info-app.component.css']
})
export class InfoAppComponent implements OnInit {

  manageApp = false;
  typeForm = '';

  @Input()
  app: UneApp;

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.manageApp = false;
  }
  
  editApp() {
    this.manageApp = true;
    this.typeForm = 'edit';
  }
  
  deleteApp() {
    this.appService.deleteApp(this.app).subscribe(
      // go back
    );
  }

  onManageApp(a: UneApp) {
      // go back
  }
}
