export class Avis {
    id: number;
    usersId: number;
    appsId: number;
    date: Date;
    note: number;
    description: string;
}
