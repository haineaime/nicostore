import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeAppsComponent } from './liste-apps/liste-apps.component';
import { ListeAvisComponent } from './liste-avis/liste-avis.component';
//import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'apps', component: ListeAppsComponent },
  { path: 'avis', component: ListeAvisComponent },
//  { path: 'user/:id', component: UserComponent },
//  { path: 'user', redirectTo: '/user/1', pathMatch: 'full' },
  { path: '', redirectTo: '/apps', pathMatch: 'full' },
 // { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
