import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionAppComponent } from './gestion-app.component';

describe('GestionAppComponent', () => {
  let component: GestionAppComponent;
  let fixture: ComponentFixture<GestionAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
