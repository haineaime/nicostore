import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatCardModule, MatIconModule } from '@angular/material';

import { AppService } from '../app.service';
import { UneApp } from '../une-app';

@Component({
  selector: 'app-gestion-app',
  templateUrl: './gestion-app.component.html',
  styleUrls: ['./gestion-app.component.css']
})
export class GestionAppComponent implements OnInit {

  formulaire: FormGroup;
  isEditing = false;

  @Input()
  app: UneApp;

  @Input()
  typeForm: string;

  @Output()
  editedApp = new EventEmitter<UneApp>();

  constructor(private formBuilder: FormBuilder,
              private appService: AppService) { }

  ngOnInit() {
    /*
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.isEditing = true;
      this.appService.getApp(id).subscribe(
        app => this.formulaire.reset(app)     // si requete rapide, formulaire sera écrasé par le code qui suit
      );
    }
    */

    if (this.typeForm !== 'edit' && this.typeForm !== 'add') {
      this.typeForm = 'add';
    }

    this.formulaire = this.formBuilder.group({
      id: [this.app.id, [Validators.required]],
      nom: [this.app.nom, [Validators.required]],
      corp: [this.app.corp, [Validators.required]],
      icone: [this.app.icone, [Validators.required]],
      categorie: [this.app.categorie, [Validators.required]],
      note: [this.app.note, [Validators.max(5), Validators.min(0), Validators.required]],
      description: [this.app.description, [Validators.required]],
      screenshots: this.formBuilder.array(this.app.screenshots),
    });
  }

  get id() {
    return this.formulaire.get('id');
  }
    get nom() {
    return this.formulaire.get('nom');
  }
  get corp() {
    return this.formulaire.get('corp');
  }
  get icone() {
    return this.formulaire.get('icone');
  }
  get categorie() {
    return this.formulaire.get('categorie');
  }
  get note() {
    return this.formulaire.get('note');
  }
  get description() {
    return this.formulaire.get('description');
  }
  get screenshots() {
    return this.formulaire.get('screenshots') as FormArray;
  }
  addScreenshot() {
    const screenshot = this.formBuilder.control('');
    /*this.formBuilder.group({
      screenshot: ''
    });*/
    this.screenshots.push(screenshot);
  }

  validateForm() {
    if (this.formulaire.valid) {
      if (this.typeForm === 'edit') {
        this.appService.updateApp(this.formulaire.value).subscribe(
          result => {
            // this.router.navigateByUrl('/')
            this.editedApp.emit(this.formulaire.value);
          }
        );
      } else {
        this.appService.createApp(this.formulaire.value).subscribe(
          result => {
            // this.router.navigateByUrl('/')
            this.editedApp.emit(this.formulaire.value);
          }
        );
      }
    }
  }
}
