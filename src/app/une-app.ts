export class UneApp {
    id: number;
    nom: string;
    corp: string;
    icone: string;
    categorie: string;
    note: number;
    description: string;
    screenshots: string[];

    constructor() {
      this.id = 0;
      this.nom = '';
      this.corp = '';
      this.icone = '';
      this.categorie = '';
      this.note = 0;
      this.description = '';
      this.screenshots = [];
    }
  }
