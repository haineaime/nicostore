import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeAppsComponent } from './liste-apps.component';

describe('ListeAppsComponent', () => {
  let component: ListeAppsComponent;
  let fixture: ComponentFixture<ListeAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
