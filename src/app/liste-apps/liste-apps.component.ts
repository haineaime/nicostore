import { Component, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { UneApp } from '../une-app';

@Component({
  selector: 'app-liste-apps',
  templateUrl: './liste-apps.component.html',
  styleUrls: ['./liste-apps.component.css']
})
export class ListeAppsComponent implements OnInit {

  listeApps: UneApp[];
  selectedApp: UneApp;
  displayApp = false;
  manageApp = false;

  typeForm = '';

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.refreshListeApps();
  }

  refreshListeApps() {
    this.displayApp = false;
    this.manageApp = false;
    this.selectedApp = null;
    this.typeForm = '';
    this.appService.getApps().subscribe(
      result => this.listeApps = result,
      error => console.error('Une erreur est survenue', error)
    );
  }

  selectApp(a: UneApp): void {
    this.displayApp = true;
    this.manageApp = false;
    this.selectedApp = a;
  }
  addApp() {
    this.displayApp = false;
    this.manageApp = true;
    this.typeForm = 'add';
    this.selectedApp = new UneApp();
  }
  editApp(a: UneApp) {
    this.displayApp = false;
    this.manageApp = true;
    this.typeForm = 'edit';
    this.selectedApp = a;
  }
  deleteApp(a: UneApp) {
    this.appService.deleteApp(a).subscribe(
      () => this.refreshListeApps()
    );
  }
  onManageApp(a: UneApp) {
    this.refreshListeApps();
    this.selectApp(a);
  }

  /*
  addScreenshot(s: string) {
    s = s.trim();
    if (!s) { return; }
    this.newApp.screenshots.push(s);
  }

  addApp() {
    this.appService.createApp(this.newApp).subscribe(
      () => this.refreshListeApps()
    );
  }

  editApp() {
    this.appService.createApp(this.newApp).subscribe(
      () => this.refreshListeApps()
    );
  }
  */

}
