import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AppService } from '../app.service';
import { Avis } from '../avis';

export interface UserV {
  id: number;
  nom: string;
}

export interface AppsV {
  id: number;
  nom: string;
}

@Component({
  selector: 'app-gestion-avis',
  templateUrl: './gestion-avis.component.html',
  styleUrls: ['./gestion-avis.component.css']
})
export class GestionAvisComponent implements OnInit {

  usersv: UserV[] = [];
  appsv: AppsV[] = [];

  selectedUser = 0;
  selectedApp = 0;
  
  uv = false;
  av = false;
  @Input() done = false;
  
  formulaire: FormGroup;
  isEditing = false;

  @Input()
  avis: Avis;

  @Input()
  typeForm: string;

  @Output()
  editedAvis = new EventEmitter<Avis>();

  constructor(private formBuilder: FormBuilder,
              private appService: AppService) { }

  ngOnInit() {

    if (this.typeForm !== 'edit' && this.typeForm !== 'add') {
      this.typeForm = 'add';
    }

    this.uv = false;
	this.av = false;
	this.done = false;

	const userPromise = this.appService.getUsers().toPromise();
	const appPromise = this.appService.getApps().toPromise();
	Promise.all([userPromise,appPromise]).then( (val) => {
      for (const u of val[0]) {
          this.usersv.push({id: u.id, nom: u.nom});
        }
	    for (const a of val[1]) {
          this.appsv.push({id: a.id, nom: a.nom});
	    }
	});

	this.selectedUser = this.avis.usersId;
	this.selectedApp = this.avis.appsId;
	
    this.formulaire = this.formBuilder.group({
      id: [this.avis.id, [Validators.required]],
      usersId: [this.avis.usersId, [Validators.required]],
      appId: [this.avis.appsId, [Validators.required]],
      date: [this.avis.date, [Validators.required]],
      note: [this.avis.note, [Validators.max(5), Validators.min(0), Validators.required]],
      description: [this.avis.description, [Validators.required]],
    });
  }
  
  get id() {
    return this.formulaire.get('id');
  }
  get usersId() {
    return this.formulaire.get('usersId');
  }
  get appsId() {
    return this.formulaire.get('appsId');
  }
  get date() {
    return this.formulaire.get('date');
  }
  get note() {
    return this.formulaire.get('note');
  }
  get description() {
    return this.formulaire.get('description');
  }

  selectUser(u: number) {
	this.formulaire.value.usersId = u;
  }
  selectApp(a: number) {
	this.formulaire.value.appId = a;
  }

  validateForm() {
    if (this.formulaire.valid) {
      if (this.typeForm === 'edit') {
        this.appService.updateAvis(this.formulaire.value).subscribe(
          result => {
            // this.router.navigateByUrl('/')
            this.editedAvis.emit(this.formulaire.value);
          }
        );
      } else {
        this.appService.createAvis(this.formulaire.value).subscribe(
          result => {
            // this.router.navigateByUrl('/')
            this.editedAvis.emit(this.formulaire.value);
          }
        );
      }
    }
  }

}
