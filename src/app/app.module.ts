import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatFormFieldModule, MatInputModule, MatButtonModule, MatCardModule, MatIconModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { ListeAppsComponent } from './liste-apps/liste-apps.component';
import { InfoAppComponent } from './info-app/info-app.component';
import { GestionAppComponent } from './gestion-app/gestion-app.component';
import { GestionAvisComponent } from './gestion-avis/gestion-avis.component';
import { ListeAvisComponent } from './liste-avis/liste-avis.component';
import { StarsComponent } from './stars/stars.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeAppsComponent,
    InfoAppComponent,
    GestionAppComponent,
    GestionAvisComponent,
    ListeAvisComponent,
    StarsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatFormFieldModule, MatInputModule, MatButtonModule, MatCardModule, MatIconModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
	MatGridListModule,
	MatTableModule,
	MatSelectModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
