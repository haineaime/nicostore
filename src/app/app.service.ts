import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UneApp } from './une-app';
import { Avis } from './avis';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  readonly BASE_APPS: string = 'apps';
  readonly BASE_AVIS: string = 'avis';
  readonly BASE_USER: string = 'users';
  readonly APP_STORE_URL: string = environment.SERVER + '/' + this.BASE_APPS;
  readonly AVIS_STORE_URL: string = environment.SERVER + '/' + this.BASE_AVIS;
  readonly USER_STORE_URL: string = environment.SERVER + '/' + this.BASE_USER;

  constructor(private http: HttpClient) { }

  getApps(): Observable<UneApp[]> {
    return this.http.get<UneApp[]>(this.APP_STORE_URL);
  }
  getApp(id: number): Observable<UneApp> {
    const url = `${this.APP_STORE_URL}/${id}`;
    return this.http.get<UneApp>(url);
  }
  createApp(a: UneApp): Observable<UneApp> {
    return this.http.post<UneApp>(this.APP_STORE_URL , a, this.httpOptions);
  }
  deleteApp(a: UneApp): Observable<UneApp> {
    const url = `${this.APP_STORE_URL}/${a.id}`;
    return this.http.delete<UneApp>(url, this.httpOptions);
  }
  updateApp(a: UneApp): Observable<UneApp> {
    const url = `${this.APP_STORE_URL}/${a.id}`;
    return this.http.put<UneApp>(url, a, this.httpOptions);
  }
  getListeAvis(): Observable<Avis[]> {
    return this.http.get<Avis[]>(this.AVIS_STORE_URL);
  }
  getAvis(id: number): Observable<Avis> {
    const url = `${this.AVIS_STORE_URL}/${id}`;
    return this.http.get<Avis>(url);
  }
  updateAvis(a: Avis): Observable<Avis> {
    const url = `${this.AVIS_STORE_URL}/${a.id}`;
    return this.http.put<Avis>(url, a, this.httpOptions);
  }
  createAvis(a: Avis): Observable<Avis> {
    const url = `${this.AVIS_STORE_URL}/${a.id}`;
    return this.http.put<Avis>(url, a, this.httpOptions);
  }
  deleteAvis(a: Avis): Observable<Avis> {
    const url = `${this.AVIS_STORE_URL}/${a.id}`;
    return this.http.delete<Avis>(url, this.httpOptions);
  }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.USER_STORE_URL);
  }
  getUser(id: number): Observable<User> {
    const url = `${this.USER_STORE_URL}/${id}`;
    return this.http.get<User>(url);
  }
  updateUser(u: User): Observable<User> {
    const url = `${this.USER_STORE_URL}/${u.id}`;
    return this.http.put<User>(url, u, this.httpOptions);
  }
  createUser(u: User): Observable<User> {
    const url = `${this.USER_STORE_URL}/${u.id}`;
    return this.http.put<User>(url, u, this.httpOptions);
  }
  deleteUser(u: User): Observable<User> {
    const url = `${this.USER_STORE_URL}/${u.id}`;
    return this.http.delete<User>(url, this.httpOptions);
  }
}
